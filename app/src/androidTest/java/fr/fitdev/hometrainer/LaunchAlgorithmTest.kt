package fr.fitdev.hometrainer

import android.content.Intent
import androidx.test.core.app.ActivityScenario
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import fr.fitdev.hometrainer.component.exercise.Exercise
import fr.fitdev.hometrainer.session.Session
import fr.fitdev.hometrainer.util.Constants
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class LaunchAlgorithmTest {

    val appContext = InstrumentationRegistry.getInstrumentation().targetContext
    var session : Session? = null

    @Before
    fun setup() {
        session = Session(1, "test", false)
    }

    @After
    fun tearDown() {
        session = null
    }

    @Test
    fun withOneExercise() {
        session?.add(Exercise(1, 0, "test", 1, 100, 100, 1, -1, 1))

        startActivity().onActivity { activity ->
            assertNull(activity.getNewExercise())
        }
    }

    @Test
    fun withTwoExercise() {
        session?.add(Exercise(1, 0, "test", 1, 100, 100, 1, -1, 1))

        val exercise = Exercise(2, 0, "test", 1, 200, 200, 1, -1, 1)
        session?.add(exercise)

        startActivity().onActivity { activity ->
            assertEquals(exercise.id, activity.getNewExercise()?.id)
        }
    }

    @Test
    fun withRepetedExercise() {
        val exercise = Exercise(1, 0, "test", 1, 100, 100, 2, -1, 1)
        session?.add(exercise)
        session?.add(Exercise(2, 0, "test", 1, 200, 200, 1, -1, 1))

        startActivity().onActivity { activity ->
            assertEquals(exercise.id, activity.getNewExercise()?.id)
        }
    }

    @Test
    fun withRepetedExercise2() {
        val exercise = Exercise(1, 0, "test", 1, 100, 100, 2, -1, 1)
        session?.add(exercise)

        val exercise2 = Exercise(2, 0, "test", 1, 200, 200, 1, -1, 1)
        session?.add(exercise2)

        startActivity().onActivity { activity ->
            assertEquals(exercise.id, activity.getNewExercise()?.id)
            activity.startNewExercise()
            assertEquals(exercise2.id, activity.getNewExercise()?.id)
        }
    }

    @Test
    fun endSession() {
        val exercise = Exercise(1, 0, "test", 1, 100, 100, 2, -1, 1)
        session?.add(exercise)

        val exercise2 = Exercise(2, 0, "test", 1, 200, 200, 1, -1, 1)
        session?.add(exercise2)

        startActivity().onActivity { activity ->
            assertEquals(exercise.id, activity.getNewExercise()?.id)
            activity.startNewExercise()
            assertEquals(exercise2.id, activity.getNewExercise()?.id)
            activity.startNewExercise()
            assertNull(activity.getNewExercise())
        }
    }

    private fun startActivity(): ActivityScenario<SessionProgressActivity> {
        val intent = Intent(appContext, SessionProgressActivity::class.java)
        intent.putExtra(Constants.EXTRA_SESSION, session!!)
        return ActivityScenario.launch(intent)
    }
}