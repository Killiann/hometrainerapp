package fr.fitdev.hometrainer

import fr.fitdev.hometrainer.util.Utils
import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class UtilsTest {

    @Test
    fun getDate_isCorrect() {
        assertEquals("5s", Utils.getDate(5000))
        assertEquals("1m 5s", Utils.getDate(5000+60000))
        assertEquals("1h 1m 5s", Utils.getDate(5000+60000+3600000))
        assertEquals("1j 1h 1m 5s", Utils.getDate(5000+60000+3600000+86400000))
    }

    @Test
    fun getColorId_isCorrect() {
        assertEquals(R.color.gray, Utils.getColorId(150))
        assertEquals(R.color.blue, Utils.getColorId(180))
        assertEquals(R.color.green, Utils.getColorId(250))
        assertEquals(R.color.yellow, Utils.getColorId(280))
        assertEquals(R.color.red, Utils.getColorId(350))
    }

    @Test
    fun numericTextIsNotValid_isCorrect() {
        assertEquals(true, Utils.numericTextIsNotValid("150", 200))
        assertEquals(false, Utils.numericTextIsNotValid("150", 20))

        assertEquals(true, Utils.numericTextIsNotValid("150", 20, 140))
        assertEquals(false, Utils.numericTextIsNotValid("150", 20, 240))
    }
}