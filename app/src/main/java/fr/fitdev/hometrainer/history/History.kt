package fr.fitdev.hometrainer.history

import android.os.Parcel
import android.os.Parcelable

/**
 * Historique d'une séance
 *
 * @property sessionId Id de la session associé
 * @property start Timestamp du début
 * @property end Timestamp de la fin
 * @property pauses Nombre de pauses
 * @property complete si la séance c'est terminé normalement
 */
data class History(
    val sessionId: Int,
    var start: Long = System.currentTimeMillis(),
    var end: Long = 0,
    var pauses: Int = 0,
    var complete: Boolean = false
) : Parcelable {

    /**
     * Crée un historique en fonction d'un [parcel]
     */
    constructor(parcel: Parcel) : this(parcel.readInt()) {
        start = parcel.readLong()
        end = parcel.readLong()
        pauses = parcel.readInt()
        complete = parcel.readByte() != 0.toByte()
    }

    /**
     * Sérialise un historique
     */
    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(sessionId)
        parcel.writeLong(start)
        parcel.writeLong(end)
        parcel.writeInt(pauses)
        parcel.writeByte(if (complete) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    /**
     * Méthodes statiques qui font appel au constructeur de la classe [History]
     */
    companion object CREATOR : Parcelable.Creator<History> {
        override fun createFromParcel(parcel: Parcel): History {
            return History(parcel)
        }

        override fun newArray(size: Int): Array<History?> {
            return arrayOfNulls(size)
        }
    }
}