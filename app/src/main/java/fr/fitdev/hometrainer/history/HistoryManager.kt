package fr.fitdev.hometrainer.history

/**
 * Manager qui permet de gérer toutes les intéraction avec la classe [History]
 *
 * @property sqlAdapter Adapteur SQL des historiques
 */
class HistoryManager private constructor(val sqlAdapter: HistorySQLAdapter = HistorySQLAdapter()) {

    /**
     * Partie statique du sigleton [HistoryManager]
     */
    companion object {

        private var instance: HistoryManager? = null

        /**
         * Retourne l'instance de [HistoryManager]
         */
        fun getInstance(): HistoryManager {
            if (instance == null)
                instance = HistoryManager()
            return instance!!
        }
    }
}