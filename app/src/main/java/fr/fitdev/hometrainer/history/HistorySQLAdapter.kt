package fr.fitdev.hometrainer.history

import fr.fitdev.hometrainer.sql.SQLAdapter
import java.lang.UnsupportedOperationException

class HistorySQLAdapter: SQLAdapter<History, Any>() {

    /**
     * Insérer un historique [generic] en base de données
     */
    override fun insert(generic: History) {
        database.writableDatabase.execSQL("INSERT INTO histories VALUES(?, ?, ?, ?, ?)",
            arrayOf(generic.start, if(generic.complete) 1 else 0, generic.pauses, generic.end, generic.sessionId))
    }

    /**
     * Retourne la liste des historiques d'une [sessionId]
     */
    fun findAll(sessionId: Int): ArrayList<History> {
        val histories = ArrayList<History>()

        database.readableDatabase.rawQuery("SELECT * FROM histories WHERE idSession = ? ORDER BY startHistory DESC", arrayOf(sessionId.toString())).use { cursor ->
            // Tant qu'il y a des lignes dans la requête
            while (cursor.moveToNext()) {

                // Construction de l'historique
                val history = History(sessionId,
                    database.getLong(cursor, "startHistory"),
                    database.getLong(cursor, "endHistory"),
                    database.getInt(cursor, "pauses"),
                    database.getInt(cursor, "complete")==1)
                histories.add(history)
            }
        }

        return histories
    }

    /**
     * Cette fonction n'a pas été implémenté, elle n'est donc pas supporté
     */
    @Throws(UnsupportedOperationException::class)
    override fun update(generic: History) {
        throw UnsupportedOperationException("Not supported")
    }

    /**
     * Cette fonction n'a pas été implémenté, elle n'est donc pas supporté
     */
    @Throws(UnsupportedOperationException::class)
    override fun delete(generic: History) {
        throw UnsupportedOperationException("Not supported")
    }

    /**
     * Cette fonction n'a pas été implémenté, elle n'est donc pas supporté
     */
    @Throws(UnsupportedOperationException::class)
    override fun find(generic: Any): History? {
        throw UnsupportedOperationException("Not supported")
    }

    /**
     * Cette fonction n'a pas été implémenté, elle n'est donc pas supporté
     */
    @Throws(UnsupportedOperationException::class)
    override fun findAll(): ArrayList<History> {
        throw UnsupportedOperationException("Not supported")
    }
}