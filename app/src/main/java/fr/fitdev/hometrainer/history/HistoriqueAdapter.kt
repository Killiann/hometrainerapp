package fr.fitdev.hometrainer.history

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import fr.fitdev.hometrainer.R
import fr.fitdev.hometrainer.util.Utils
import java.text.SimpleDateFormat

/**
 * Adapteur qui permet de remplir les information des [histories] dans les item_history pour l'affichage
 *
 * @property histories Listes des historiques d'une séance
 */
class HistoryAdapter(private val histories: List<History>) : RecyclerView.Adapter<HistoryView>() {

    /**
     * Création de la vue en fonction du layout item_history
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HistoryView {
        val viewItem = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_history, parent, false)
        return HistoryView(viewItem)
    }

    /**
     * Méthode qui définit les informations des [histories] d'une séance
     */
    override fun onBindViewHolder(holder: HistoryView, position: Int) {
        val history = histories[position]

        holder.cardView.tag = position
        holder.name.text = "${SimpleDateFormat("dd/MM/yyyy 'à' HH:mm").format(history.start)}"
        holder.pauses.text = "Nombre de pauses: ${history.pauses}"
        holder.complete.text = if (history.complete) "Séance terminé" else "Séance interrompu"
        holder.duration.text = "Durée: ${Utils.getDate(history.end - history.start)}"
    }

    /**
     * Retourne le nombre de [histories]
     */
    override fun getItemCount(): Int = histories.size
}

/**
 * Classe qui stocke la liste des éléments dans le layout item_history
 */
class HistoryView(view: View) : RecyclerView.ViewHolder(view) {
    val cardView: CardView = view.findViewById(R.id.history_card_view)
    val name: TextView = view.findViewById(R.id.history_time)
    val pauses: TextView = view.findViewById(R.id.history_pauses)
    val complete: TextView = view.findViewById(R.id.history_finish)
    val duration: TextView = view.findViewById(R.id.history_duree)
}