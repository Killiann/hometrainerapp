package fr.fitdev.hometrainer

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import com.google.android.material.floatingactionbutton.FloatingActionButton
import fr.fitdev.hometrainer.component.Component
import fr.fitdev.hometrainer.component.block.Block
import fr.fitdev.hometrainer.popup.block.PopupUpdateBlock
import fr.fitdev.hometrainer.popup.exercise.PopupCreateExercise
import fr.fitdev.hometrainer.util.Constants

class BlockDetailActivity : DetailActivity<Block, Component>(R.layout.activity_block_detail) {

    private lateinit var popupCreateExercise: PopupCreateExercise<Block, Component>
    private lateinit var popupUpdateBlock: PopupUpdateBlock<Block, Component>

    private var blockIndex: Int = 0

    /**
     * Création de l'activité
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        //Récupération des informations de la dernière activité
        key = intent.getParcelableExtra(Constants.EXTRA_BLOCK)
        blockIndex = intent.getIntExtra(Constants.EXTRA_BLOCK_INDEX, -1)

        components = key.exercises
        super.onCreate(savedInstanceState)

        // Init des popups
        popupCreateExercise = PopupCreateExercise(this)
        popupUpdateBlock = PopupUpdateBlock(this)

        // Si l'utilisateur clique sur le bouton de création alors ça affiche le popup
        findViewById<FloatingActionButton>(R.id.button_create).setOnClickListener { popupCreateExercise.show() }
    }

    override fun setupToolbar() {
        // On change le titre de la toolbar avec le nom du bloc
        toolbar.title = key.name
    }

    override fun onBackPressed() {
        // Quand l'utilisateur ferme l'activité on renvoie le bloc à jour pour garder un cache vakude
        intent = Intent()
        intent.putExtra(Constants.EXTRA_BLOCK, key)
        intent.putExtra(Constants.EXTRA_BLOCK_INDEX, blockIndex)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    /**
     * Création du menu d'option (Petits logos à droite du titre de la toolbar)
     */
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_block, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == android.R.id.home) {
            this.onBackPressed()
            return true
        } else if (item.itemId == R.id.action_edit) {
            // On affiche le popup de mise à jour
            popupUpdateBlock.show(key)
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}