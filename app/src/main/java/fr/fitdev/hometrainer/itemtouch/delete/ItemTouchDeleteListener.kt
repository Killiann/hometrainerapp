package fr.fitdev.hometrainer.itemtouch.delete

import androidx.recyclerview.widget.RecyclerView

/**
 * Canal d'écoute apppelé quand un item est supprimé
 */
fun interface ItemTouchDeleteListener {
    fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int)
}