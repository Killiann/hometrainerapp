package fr.fitdev.hometrainer.itemtouch.move

import androidx.recyclerview.widget.RecyclerView

/**
 * Canal d'écoute apppelé quand un item est 'drag & drop'
 */
fun interface ItemTouchMoveListener {
    fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder,
        target: RecyclerView.ViewHolder)
}