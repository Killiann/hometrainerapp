package fr.fitdev.hometrainer.itemtouch.delete

import android.view.View
import android.widget.RelativeLayout
import androidx.recyclerview.widget.RecyclerView
import fr.fitdev.hometrainer.R

/**
 * Classe qui stocke la liste des éléments pour la suppression d'un item d'un RecyclerView
 */
open class DeletableView(view: View) : RecyclerView.ViewHolder(view) {
    val viewBackground: RelativeLayout = view.findViewById(R.id.delete_background)
    val viewForeground: RelativeLayout = view.findViewById(R.id.card_foreground)
}