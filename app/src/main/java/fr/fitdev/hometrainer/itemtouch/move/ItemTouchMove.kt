package fr.fitdev.hometrainer.itemtouch.move

import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import java.util.*
import kotlin.collections.ArrayList

/**
 * Classe utililaire qui gère automatiquement l'annimation de 'drag & drop' d'un item
 *
 * @param T Type de l'item qui va être 'drag & drop'
 * @property listener Canal d'écoute appelé une fois que l'item a été 'drop'
 */
class ItemTouchMove<T>(private val list: ArrayList<T>, private val listener: ItemTouchMoveListener) :
    ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP.or(ItemTouchHelper.DOWN), 0) {

    /**
     * Appel le [listener]
     */
    override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder,
                        target: RecyclerView.ViewHolder): Boolean {
        listener.onMove(recyclerView, viewHolder, target)

        // Echange les positions des 2 item de la recyclerView
        val startIndex = viewHolder.adapterPosition
        val endIndex = target.adapterPosition

        Collections.swap(list, startIndex, endIndex)

        // Applique les changements à la recyclerView
        recyclerView.adapter?.notifyItemMoved(startIndex, endIndex)
        return true
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
    }
}

