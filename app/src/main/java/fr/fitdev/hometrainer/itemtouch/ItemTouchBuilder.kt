package fr.fitdev.hometrainer.itemtouch

import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import fr.fitdev.hometrainer.itemtouch.delete.ItemTouchDelete
import fr.fitdev.hometrainer.itemtouch.delete.ItemTouchDeleteListener
import fr.fitdev.hometrainer.itemtouch.move.ItemTouchMove
import fr.fitdev.hometrainer.itemtouch.move.ItemTouchMoveListener

/**
 * Builder qui permet d'associer des actions à des items d'un RecyclerView
 *
 * @property recyclerView
 */
class ItemTouchBuilder(private val recyclerView: RecyclerView) {

    /**
     * Associe au [recyclerView] une action de suppression d'un item avec [listener] comme sortie
     */
    fun delete(listener: ItemTouchDeleteListener): ItemTouchBuilder {
        ItemTouchHelper(ItemTouchDelete(listener)).attachToRecyclerView(recyclerView)
        return this
    }

    /**
     * Associe au [recyclerView] une action de 'drag & drop' d'un item avec [listener] comme sortie
     *
     * @param T type de l'item
     */
    fun <T> move(list: ArrayList<T>, listener: ItemTouchMoveListener): ItemTouchBuilder {
        ItemTouchHelper(ItemTouchMove(list, listener)).attachToRecyclerView(recyclerView)
        return this
    }
}