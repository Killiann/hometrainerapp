package fr.fitdev.hometrainer

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.TextView
import android.widget.Toast
import com.google.android.material.floatingactionbutton.FloatingActionButton
import fr.fitdev.hometrainer.component.*
import fr.fitdev.hometrainer.component.block.Block
import fr.fitdev.hometrainer.component.exercise.Exercise
import fr.fitdev.hometrainer.popup.block.PopupCreateBlock
import fr.fitdev.hometrainer.popup.exercise.PopupCreateExercise
import fr.fitdev.hometrainer.popup.session.PopupUpdateSession
import fr.fitdev.hometrainer.session.Session
import fr.fitdev.hometrainer.session.SessionManager
import fr.fitdev.hometrainer.util.Constants
import fr.fitdev.hometrainer.util.Utils

class SessionDetailActivity : DetailActivity<Session, Component>(R.layout.activity_session_detail) {

    private lateinit var popupCreateExercise: PopupCreateExercise<Session, Component>
    private lateinit var popupCreateBlockBlock: PopupCreateBlock<Session, Component>
    private lateinit var popupUpdateSession: PopupUpdateSession

    /**
     * Création de l'activité
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        //Récupération des informations de la dernière activité
        key = intent.getParcelableExtra(Constants.EXTRA_SESSION)

        components = key.components
        super.onCreate(savedInstanceState)

        // Quand l'utilisateur clique sur le bouton de lancement de séance on la lance
        findViewById<FloatingActionButton>(R.id.start_session).setOnClickListener {

            // Si la séance n'a aucun exercice alors on affiche une erreur
            if(components.stream().noneMatch { component -> (component is Exercise) || (component as Block).exercises.isNotEmpty() }) {
                Toast.makeText(this, "Vous devez avoir au minimum 1 exercice pour lancer une séance",
                    Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }

            // On lance l'activité de progression de la session
            val intent = Intent(this, SessionProgressActivity::class.java)
            intent.putExtra(Constants.EXTRA_SESSION, key)
            startActivity(intent)
        }

        // Init des popups
        popupCreateExercise = PopupCreateExercise(this)
        popupUpdateSession = PopupUpdateSession(this)
        popupCreateBlockBlock = PopupCreateBlock(this)

        // Actions des popups
        findViewById<com.github.clans.fab.FloatingActionButton>(R.id.button_create).setOnClickListener { popupCreateExercise.show() }
        findViewById<com.github.clans.fab.FloatingActionButton>(R.id.button_create_group).setOnClickListener { popupCreateBlockBlock.show() }

        findViewById<TextView>(R.id.duration).text = "Durée: ${Utils.getDateWithoutSeconds(key.getDurationInMillis())}"
    }

    override fun setupToolbar() {
        toolbar.title = key.name
    }

    /**
     * Création du menu d'option (Petits logos à droite du titre de la toolbar)
     */
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_history, menu)
        setupOptionItem()
        return true
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        setupOptionItem()
        return super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_history -> {
                // Ouverture activité historique
                val intent = Intent(this, SessionHistoryActivity::class.java)
                intent.putExtra(Constants.EXTRA_SESSION, key)
                startActivity(intent)
                return true
            }
            R.id.action_update_name -> {
                // On affiche le popup de mise à jour de la séance
                popupUpdateSession.show()
                return true
            }
            R.id.action_favorite -> {
                // Metttre en favori ou non une séance
                key.favorite = !key.favorite
                SessionManager.getInstance().sqlAdapter.update(key)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    /**
     * Appelé quand on revient de l'activité [BlockDetailActivity] pour mettre à jour le cache
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(resultCode == Activity.RESULT_OK && data != null) {
            // Récupération des informations
            val block: Block = data.getParcelableExtra(Constants.EXTRA_BLOCK)
            val blockIndex: Int = data.getIntExtra(Constants.EXTRA_BLOCK_INDEX, -1)

            // Mise à jour du cache
            key.components[blockIndex] = block
            components[blockIndex] = block

            // Mise à jour du recyclerView
            componentAdapter.notifyItemChanged(blockIndex)
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    /**
     * Modification du menu d'option en fonction de la session
     */
    private fun setupOptionItem() {
        toolbar.menu.findItem(R.id.action_favorite).title =
            if (key.favorite) "Enlever des favoris" else "Ajouter en favori"
    }
}