package fr.fitdev.hometrainer.util

object Constants {

    const val EXTRA_SESSION = "hometrainer.session"

    const val EXTRA_HISTORY = "hometrainer.history"

    const val EXTRA_BLOCK = "hometrainer.block"
    const val EXTRA_BLOCK_INDEX = "hometrainer.blockIndex"
}