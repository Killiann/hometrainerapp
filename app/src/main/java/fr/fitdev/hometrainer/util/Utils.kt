package fr.fitdev.hometrainer.util

import fr.fitdev.hometrainer.R
import java.lang.Exception

object Utils {

    /**
     * Retourne une intervalle entre 2 timestamp formatté
     */
    fun getDate(millis: Long): String {
        val millisInDay: Long = 86400000
        val millisInHour: Long = 3600000
        val millisInMinute: Long = 60000
        val millisInSecond: Long = 1000

        val days = millis / millisInDay
        val daysDivisionResidueMillis = millis - days * millisInDay

        val hours = daysDivisionResidueMillis / millisInHour
        val hoursDivisionResidueMillis = daysDivisionResidueMillis - hours * millisInHour

        val minutes = hoursDivisionResidueMillis / millisInMinute
        val minutesDivisionResidueMillis = hoursDivisionResidueMillis - minutes * millisInMinute

        val seconds = minutesDivisionResidueMillis / millisInSecond
        var date = ""
        if (days > 0) {
            date = "${days}j "
        }
        if (hours > 0) {
            date = "$date${hours}h "
        }
        if (minutes > 0) {
            date = "$date${minutes}m "
        }
        return "$date${seconds}s"
    }

    /**
     * Retourne une intervalle entre 2 timestamp formatté sans les secondes
     */
    fun getDateWithoutSeconds(millis: Long): String {
        val millisInDay: Long = 86400000
        val millisInHour: Long = 3600000
        val millisInMinute: Long = 60000
        val millisInSecond: Long = 1000

        val days = millis / millisInDay
        val daysDivisionResidueMillis = millis - days * millisInDay

        val hours = daysDivisionResidueMillis / millisInHour
        val hoursDivisionResidueMillis = daysDivisionResidueMillis - hours * millisInHour

        val minutes = hoursDivisionResidueMillis / millisInMinute
        val minutesDivisionResidueMillis = hoursDivisionResidueMillis - minutes * millisInMinute

        val seconds = minutesDivisionResidueMillis / millisInSecond
        var date = ""
        if (hours > 0) {
            date = "$date${hours}h"
        }
        if (minutes > 0) {
            date = "$date${minutes}m"
        }
        return date
    }

    /**
     * Retourne une couleur associé à la puissance d'un exercice
     */
    fun getColorId(power: Int): Int {
        return when {
            power==0 -> R.color.white
            power<=150 -> R.color.gray
            power<200 -> R.color.blue
            power<260 -> R.color.green
            power<300 -> R.color.yellow
            else -> R.color.red
        }
    }

    /**
     * Retourne Vrai si un nombre est invalide
     */
    fun numericTextIsNotValid(test: String, min: Int, max: Int = Int.MAX_VALUE): Boolean {
        var i: Int
        try {
            i = Integer.parseInt(test)
        } catch (e: Exception) {
            return true
        }
        return i < min || i > max
    }
}