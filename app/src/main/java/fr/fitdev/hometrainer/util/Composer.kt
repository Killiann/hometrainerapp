package fr.fitdev.hometrainer.util

import fr.fitdev.hometrainer.component.Component

/**
 * Classe qui est implémenter quand un objet peut stocké des [T]
 *
 * @param T le component stocké par le [Composer]
 */
interface Composer<T: Component> {

    /**
     * Ajouter une [generic] dans la liste du [Composer]
     */
    fun add(generic: T)

    /**
     * Retourne le nombre de [T] du [Composer]
     */
    fun size(): Int

    /**
     * Retourne l'id de la session du [Composer]
     */
    fun getSession(): Int
}