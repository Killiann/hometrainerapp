package fr.fitdev.hometrainer.popup.session

import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import fr.fitdev.hometrainer.MainActivity
import fr.fitdev.hometrainer.R
import fr.fitdev.hometrainer.popup.Popup
import fr.fitdev.hometrainer.session.Session
import fr.fitdev.hometrainer.session.SessionManager
import java.lang.Exception

/**
 * Popup pour créer une [Session]
 *
 * @property activity Activité actuel
 */
class PopupCreateSession(activity: MainActivity) :
// Si l'utilisateur appuit sur le bouton de création
    Popup<MainActivity>(activity, R.layout.popup_session_create) {

    override fun processAfterShow() {
        dialog.findViewById<Button>(R.id.button_create)?.setOnClickListener {
            val editText = dialog.findViewById<EditText>(R.id.session_name)!!

            when {
                // Si le champ du nom est invalide on affiche une erreur
                editText.text.isEmpty() -> editText.error = "Ce nom de séance n'est pas valide"
                editText.text.length >= 100 -> editText.error = "Ce nom de séance est trop grand"

                // Création de la session et fermeture du dialogue
                else -> {
                    try {
                        val session = Session(-1, editText.text.toString(), false)
                        SessionManager.getInstance().sqlAdapter.insert(session)
                        activity.sessions.add(session)
                        activity.sessionAdapter.notifyDataSetChanged()
                    } catch (e: Exception) {
                        Toast.makeText(activity, "Ce nom de séance existe déjà !",
                            Toast.LENGTH_LONG).show()
                    }
                    editText.text.clear()
                    dialog.dismiss()
                }
            }
        }
    }
}