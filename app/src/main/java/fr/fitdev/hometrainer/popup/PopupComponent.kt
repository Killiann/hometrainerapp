package fr.fitdev.hometrainer.popup

import fr.fitdev.hometrainer.DetailActivity
import fr.fitdev.hometrainer.component.Component
import fr.fitdev.hometrainer.util.Composer

/**
 * Classe abstraite pour créer des popups qui sont en lien avec des composants & components
 *
 * @param K composeur qui stocke le [T]
 * @param T component stocké dans [K]
 * @property activity Activité actuelle
 */
abstract class PopupComponent<K : Composer<T>, T : Component>(
    activity: DetailActivity<K, T>,
    resource: Int
) : Popup<DetailActivity<K, T>>(activity, resource) {

    protected lateinit var component: T
    protected var index: Int = -1

    /**
     * Affiche un [T] avec un [index] par défaut = à -1
     */
    fun show(generic: T, index: Int = -1) {
        component = generic
        this.index = index

        this.show()
    }
}