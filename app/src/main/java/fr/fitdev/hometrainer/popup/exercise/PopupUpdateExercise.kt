package fr.fitdev.hometrainer.popup.exercise

import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import fr.fitdev.hometrainer.DetailActivity
import fr.fitdev.hometrainer.R
import fr.fitdev.hometrainer.component.Component
import fr.fitdev.hometrainer.component.ComponentManager
import fr.fitdev.hometrainer.component.exercise.Exercise
import fr.fitdev.hometrainer.popup.PopupComponent
import fr.fitdev.hometrainer.util.Composer
import fr.fitdev.hometrainer.util.Utils
import java.lang.Exception
/**
 * Popup pour mettre à jour un [Exercise]
 *
 * @param K composeur qui stocke le [T]
 * @param T component stocké dans [K]
 * @property activity Activité actuel
 */
class PopupUpdateExercise<K : Composer<T>, T : Component>(activity: DetailActivity<K, T>) :
    PopupComponent<K, T>(activity, R.layout.popup_exercise_update) {

    override fun processAfterShow() {
        if (component !is Exercise) return
        val exercise = component as Exercise

        val nameText = dialog.findViewById<EditText>(R.id.exercise_name)!!
        val powerText = dialog.findViewById<EditText>(R.id.exercise_power)!!
        val frequencyText = dialog.findViewById<EditText>(R.id.exercise_frequency)!!
        val durationText = dialog.findViewById<EditText>(R.id.exercise_duration)!!
        val repetitionText = dialog.findViewById<EditText>(R.id.exercise_repetition)!!

        // Auto complétion des champs
        nameText.setText(exercise.name)
        powerText.setText(exercise.power.toString())
        frequencyText.setText(exercise.frequency.toString())
        durationText.setText(exercise.duration.toString())
        repetitionText.setText(exercise.repetition.toString())

        // Si l'utilisateur appuit sur le bouton de mise à jour
        dialog.findViewById<Button>(R.id.button_create)?.setOnClickListener {
            when {
                // Si le champ du nom est invalide on affiche une erreur
                nameText.text.isEmpty() || nameText.text.length > 100 -> nameText.error =
                    "Ce nom d'exercice est invalide"

                // Si le champ de durée est invalide on affiche une erreur
                durationText.text.isEmpty() || Utils.numericTextIsNotValid(durationText.text.toString(), 1) -> durationText.error =
                    "La durée est invalide"

                // Si le champ de puissance est invalide on affiche une erreur
                powerText.text.isEmpty() || Utils.numericTextIsNotValid(powerText.text.toString(), 0) -> powerText.error =
                    "La puissance est invalide"

                // Si le champ de fréquence est invalide on affiche une erreur
                frequencyText.text.isEmpty() || Utils.numericTextIsNotValid(frequencyText.text.toString(), 0) -> frequencyText.error =
                    "La vitesse est invalide"

                // Si le champ de répétition est invalide on affiche une erreur
                repetitionText.text.isEmpty() || Utils.numericTextIsNotValid(repetitionText.text.toString(), 1) -> repetitionText.error =
                    "Le nombre de répétitions est invalide"

                // Mise à jour de l'exercice et fermeture du dialogue
                else -> {
                    try {
                        val exerciseModified = Exercise(
                            exercise.id,
                            exercise.index,
                            nameText.text.toString(),
                            Integer.parseInt(durationText.text.toString()),
                            Integer.parseInt(powerText.text.toString()),
                            Integer.parseInt(frequencyText.text.toString()),
                            Integer.parseInt(repetitionText.text.toString()),
                            -1,
                            exercise.sessionId
                        )

                        ComponentManager.getInstance().exerciseSQLAdapter.update(exerciseModified)

                        // On ne modifie pas exercise avant le update pour eviter de corrompre le cache s'il y a une erreur dans la requête d'update
                        exercise.name = exerciseModified.name
                        exercise.power = exerciseModified.power
                        exercise.frequency = exerciseModified.frequency
                        exercise.duration = exerciseModified.duration
                        exercise.repetition = exerciseModified.repetition

                        activity.componentAdapter.notifyItemChanged(index)
                    } catch (e: Exception) {
                        Toast.makeText(activity, "Erreur interne durant la modification..",
                            Toast.LENGTH_LONG).show()
                    }

                    dialog.dismiss()
                }
            }
        }
    }

}