package fr.fitdev.hometrainer.popup

import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity

/**
 * Classe abstraite pour créer des popups
 *
 * @param A activité
 * @property activity Activité actuelle
 */
abstract class Popup<A: AppCompatActivity>(protected val activity: A, resource: Int) {

    var active = true

    private var dialogBuilder: AlertDialog.Builder = AlertDialog.Builder(activity)
    protected var dialog: AlertDialog

    /**
     * Création du AlertDialog pour le popup
     */
    init {
        dialogBuilder.setView(activity.layoutInflater.inflate(resource, null))
        dialog = dialogBuilder.create()
    }

    fun show() {
        if(!active) return

        dialog.show()
        processAfterShow()
    }

    /**
     * Méthodes exécuté après l'affichage du popup
     */
    abstract fun processAfterShow()
}