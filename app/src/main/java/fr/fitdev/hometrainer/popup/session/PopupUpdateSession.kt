package fr.fitdev.hometrainer.popup.session

import android.widget.Button
import android.widget.EditText
import fr.fitdev.hometrainer.R
import fr.fitdev.hometrainer.SessionDetailActivity
import fr.fitdev.hometrainer.popup.Popup
import fr.fitdev.hometrainer.session.Session
import fr.fitdev.hometrainer.session.SessionManager
import java.lang.Exception

/**
 * Popup pour mettre à jour une [Session]
 *
 * @property activity Activité actuel
 */
class PopupUpdateSession(activity: SessionDetailActivity) :
    Popup<SessionDetailActivity>(activity, R.layout.popup_session_update) {

    override fun processAfterShow() {
        // Si l'utilisateur appuit sur le bouton de modification
        dialog.findViewById<Button>(R.id.button_create)?.setOnClickListener {
            val editText = dialog.findViewById<EditText>(R.id.session_name)!!

            when {
                // Si le champ du nom est invalide on affiche une erreur
                editText.text.isEmpty() -> editText.error = "Ce nom de séance n'est pas valide"
                editText.text.length >= 100 -> editText.error = "Ce nom de séance est trop grand"

                // Mise à jour de la session et fermeture du dialogue
                else -> {
                    try {
                        val session = Session(
                            activity.key.id,
                            editText.text.toString(),
                            activity.key.favorite
                        )
                        SessionManager.getInstance().sqlAdapter.update(session)
                        activity.toolbar.title = session.name
                    } catch (e: Exception) {
                        editText.error = "Ce nom de séance existe déjà"
                        return@setOnClickListener
                    }
                    editText.text.clear()
                    dialog.dismiss()
                }
            }
        }
    }
}