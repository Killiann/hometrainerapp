package fr.fitdev.hometrainer.popup.timer

import android.content.Intent
import android.os.SystemClock
import android.widget.Button
import android.widget.Chronometer
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.cardview.widget.CardView
import fr.fitdev.hometrainer.MainActivity
import fr.fitdev.hometrainer.R
import fr.fitdev.hometrainer.SessionProgressActivity
import fr.fitdev.hometrainer.popup.Popup
import fr.fitdev.hometrainer.timer.TimerState

/**
 * Popup pour une pause
 *
 * @property activity Activité actuel
 */
class PopupPause(activity: SessionProgressActivity) :
    Popup<SessionProgressActivity>(activity, R.layout.popup_session_progress) {

    init {
        dialog = AlertDialog.Builder(activity)
            .setView(activity.layoutInflater.inflate(R.layout.popup_session_progress, null))
            .setCancelable(false)
            .create()
    }

    override fun processAfterShow() {
        // Lancement du chronomètre de la pause
        val chronometer = dialog.findViewById<Chronometer>(R.id.popup_countdown)!!
        if(activity.timer.timerState != TimerState.PAUSE) {
            chronometer.base = SystemClock.elapsedRealtime()
            chronometer.start()
        }

        // Si l'utilisateur continue sa séance
        dialog.findViewById<Button>(R.id.button_continue)?.setOnClickListener {
            // Stop du chronomètre + fermeture du dialogue
            chronometer.stop()
            chronometer.base = SystemClock.elapsedRealtime()
            activity.timer.startTimer()
            dialog.dismiss()
        }

        // Si l'utilisateur arrête sa séance
        dialog.findViewById<Button>(R.id.button_stop)?.setOnClickListener {
            // fermeture du dialogue + Retour sur l'activité principale
            dialog.dismiss()
            activity.finish()
            val intent = Intent(activity, MainActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            activity.startActivity(intent)
        }

        dialog.findViewById<TextView>(R.id.time)?.text = "Temps restant: ${activity.progressText.text}"


        val cardView = dialog.findViewById<CardView>(R.id.exercise_cardview) ?: return
        cardView.setCardBackgroundColor(activity.findViewById<CardView>(R.id.exercise_progress_cardview).cardBackgroundColor)

        cardView.findViewById<TextView>(R.id.actual_exercise).text = activity.findViewById<TextView>(R.id.actual_exercise).text
        cardView.findViewById<TextView>(R.id.actual_repetition).text = activity.findViewById<TextView>(R.id.actual_repetition).text
        cardView.findViewById<TextView>(R.id.actual_power).text = activity.findViewById<TextView>(R.id.actual_power).text
        cardView.findViewById<TextView>(R.id.actual_frequency).text = activity.findViewById<TextView>(R.id.actual_frequency).text
    }
}