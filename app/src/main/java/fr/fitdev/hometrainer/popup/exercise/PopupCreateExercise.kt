package fr.fitdev.hometrainer.popup.exercise

import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import fr.fitdev.hometrainer.DetailActivity
import fr.fitdev.hometrainer.R
import fr.fitdev.hometrainer.component.Component
import fr.fitdev.hometrainer.component.ComponentManager
import fr.fitdev.hometrainer.component.block.Block
import fr.fitdev.hometrainer.component.exercise.Exercise
import fr.fitdev.hometrainer.popup.PopupComponent
import fr.fitdev.hometrainer.util.Composer
import fr.fitdev.hometrainer.util.Utils
import java.lang.Exception

/**
 * Popup pour créer un [Exercise]
 *
 * @param K composeur qui stocke le [T]
 * @param T component stocké dans [K]
 * @property activity Activité actuel
 */
class PopupCreateExercise<K : Composer<T>, T : Component>(activity: DetailActivity<K, T>) :
    PopupComponent<K, T>(activity, R.layout.popup_exercise_create) {

    override fun processAfterShow() {
        val repetitionText = dialog.findViewById<EditText>(R.id.exercise_repetition)!!
        repetitionText.text.clear()

        // Si l'utilisateur appuit sur le bouton de création
        dialog.findViewById<Button>(R.id.button_create)?.setOnClickListener {
            val nameText = dialog.findViewById<EditText>(R.id.exercise_name)!!
            val powerText = dialog.findViewById<EditText>(R.id.exercise_power)!!
            val frequencyText = dialog.findViewById<EditText>(R.id.exercise_frequency)!!
            val durationText = dialog.findViewById<EditText>(R.id.exercise_duration)!!

            when {
                // Si le champ du nom est invalide on affiche une erreur
                nameText.text.isEmpty() || nameText.text.length > 100 -> nameText.error =
                    "Ce nom d'exercice est invalide"

                // Si le champ de durée est invalide on affiche une erreur
                durationText.text.isEmpty() || Utils.numericTextIsNotValid(durationText.text.toString(), 1, 240) -> durationText.error =
                    "La durée est invalide"

                // Si le champ de puissance est invalide on affiche une erreur
                powerText.text.isEmpty() || Utils.numericTextIsNotValid(powerText.text.toString(), 0) -> powerText.error =
                    "La puissance est invalide"

                // Si le champ de fréquence est invalide on affiche une erreur
                frequencyText.text.isEmpty() || Utils.numericTextIsNotValid(frequencyText.text.toString(), 0) -> frequencyText.error =
                    "La vitesse est invalide"

                // Si le champ de répétition est invalide on affiche une erreur
                repetitionText.text.isEmpty() || Utils.numericTextIsNotValid(repetitionText.text.toString(), 1) -> repetitionText.error =
                    "Le nombre de répétitions est invalide"

                // Création de l'exercice et fermeture du dialogue
                else -> {

                    try {
                        // Condition ternaire pour vérifier si l'exercice est créé dans un bloc
                        val blockId = if (activity.key is Block) (activity.key as Block).id else -1

                        val exercise = Exercise(
                            -1,
                            activity.key.size(),
                            nameText.text.toString(),
                            Integer.parseInt(durationText.text.toString()),
                            Integer.parseInt(powerText.text.toString()),
                            Integer.parseInt(frequencyText.text.toString()),
                            Integer.parseInt(repetitionText.text.toString()),
                            blockId,
                            activity.key.getSession()
                        )

                        ComponentManager.getInstance().exerciseSQLAdapter.insert(exercise)
                        activity.key.add(exercise as T)
                        activity.componentAdapter.notifyItemInserted(exercise.index)
                    } catch (e: Exception) {
                        Toast.makeText(activity, "Erreur interne durant la création..",
                            Toast.LENGTH_LONG).show()
                    }

                    nameText.text.clear()
                    powerText.text.clear()
                    frequencyText.setText("0")
                    durationText.text.clear()
                    repetitionText.text.clear()
                    dialog.dismiss()
                }
            }
        }
    }
}