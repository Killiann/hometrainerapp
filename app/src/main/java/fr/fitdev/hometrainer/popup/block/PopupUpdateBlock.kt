package fr.fitdev.hometrainer.popup.block

import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import fr.fitdev.hometrainer.DetailActivity
import fr.fitdev.hometrainer.R
import fr.fitdev.hometrainer.component.Component
import fr.fitdev.hometrainer.component.ComponentManager
import fr.fitdev.hometrainer.component.block.Block
import fr.fitdev.hometrainer.popup.PopupComponent
import fr.fitdev.hometrainer.util.Composer
import fr.fitdev.hometrainer.util.Utils
import java.lang.Exception

/**
 * Popup pour mettre à jour un [Block]
 *
 * @param K composeur qui stocke le [T]
 * @param T component stocké dans [K]
 * @property activity Activité actuel
 */
class PopupUpdateBlock<K : Composer<T>, T : Component>(activity: DetailActivity<K, T>) :
    PopupComponent<K, T>(activity, R.layout.popup_block_update) {

    override fun processAfterShow() {
        if (component !is Block) return

        // Auto complétion des champs
        val repetitionText = dialog.findViewById<EditText>(R.id.block_repetition)!!
        repetitionText.text.clear()
        repetitionText.setText(component.repetition.toString())

        val nameText = dialog.findViewById<EditText>(R.id.block_name)!!
        nameText.setText(component.name)

        // Si l'utilisateur appuit sur le bouton de mise à jour
        dialog.findViewById<Button>(R.id.button_create)?.setOnClickListener {
            when {
                // Si le champ du nom est invalide on affiche une erreur
                nameText.text.isEmpty() || nameText.text.length > 100 -> nameText.error =
                    "Ce nom d'exercice est invalide"

                // Si le champ de répétition est invalide on affiche une erreur
                repetitionText.text.isEmpty() || Utils.numericTextIsNotValid(repetitionText.text.toString(), 1) -> repetitionText.error =
                    "Le nombre de répétitions est invalide"

                // Mise à jour du block et fermeture du dialogue
                else -> {
                    try {
                        val blockUpdate = Block(
                            component.id,
                            component.index,
                            nameText.text.toString(),
                            Integer.parseInt(repetitionText.text.toString()),
                            activity.key.getSession()
                        )
                        ComponentManager.getInstance().blockSQLAdapter.update(blockUpdate)

                        component.name = blockUpdate.name
                        component.repetition = blockUpdate.repetition

                        activity.toolbar.title = component.name
                    } catch (e: Exception) {
                        Toast.makeText(activity, "Erreur interne durant la création..",
                            Toast.LENGTH_LONG).show()
                    }

                    nameText.text.clear()
                    repetitionText.text.clear()
                    dialog.dismiss()
                }
            }
        }
    }
}