package fr.fitdev.hometrainer

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import fr.fitdev.hometrainer.component.*
import fr.fitdev.hometrainer.component.block.BlockClickListener
import fr.fitdev.hometrainer.component.exercise.ExerciseClickListener
import fr.fitdev.hometrainer.itemtouch.ItemTouchBuilder
import fr.fitdev.hometrainer.util.Composer
import fr.fitdev.hometrainer.itemtouch.delete.ItemTouchDelete
import fr.fitdev.hometrainer.itemtouch.delete.ItemTouchDeleteListener
import fr.fitdev.hometrainer.itemtouch.move.ItemTouchMove
import fr.fitdev.hometrainer.itemtouch.move.ItemTouchMoveListener

/**
 * Classe abstraite pour traiter le détail d'un [Composer]
 *
 * @param K composeur qui stocke le [T]
 * @param T component stocké dans [K]
 * @property layoutResID Id du layout à afficher
 */
abstract class DetailActivity<K : Composer<T>, T : Component>(private val layoutResID: Int) :
    AppCompatActivity() {

    private val componentSQLAdapter = ComponentManager.getInstance().componentSQLAdapter

    lateinit var toolbar: Toolbar
    lateinit var componentAdapter: ComponentAdapter
    lateinit var components: ArrayList<T>

    lateinit var key: K

    /**
     * Création de l'activité
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // On affiche le layout
        setContentView(layoutResID)

        // On configure la toolbar
        toolbar = findViewById(R.id.toolbar)
        setupToolbar()
        setSupportActionBar(toolbar)
        // Ajout du retour en arrière (Petite flèche avant le titre de la toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        // Configuration du l'adapteur et de son recyclerView
        componentAdapter = ComponentAdapter(components, ExerciseClickListener(this), BlockClickListener(this))
        val recyclerView = findViewById<RecyclerView>(R.id.exercise_recyclerview)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = componentAdapter

        // Configuration des actions possibles sur les item du recyclerView
        ItemTouchBuilder(recyclerView).delete { viewHolder: RecyclerView.ViewHolder, _: Int ->
            val index: Int = viewHolder.adapterPosition
            val exercise = components[index]
            componentSQLAdapter.delete(exercise)
            components.removeAt(index)
            componentAdapter.notifyItemRemoved(index)
        }
            .move(components) { _: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder ->
                val startIndex = viewHolder.adapterPosition
                val endIndex = target.adapterPosition

                val startExercise = components[startIndex]
                startExercise.index = endIndex
                (viewHolder as ComponentView).cardView.tag = endIndex

                val endExercise = components[endIndex]
                endExercise.index = startIndex
                (target as ComponentView).cardView.tag = startIndex

                componentSQLAdapter.update(startExercise)
                componentSQLAdapter.update(endExercise)
            }
    }

    /**
     * Méthode abstraite pour configurer la toolbar
     */
    abstract fun setupToolbar()
}