package fr.fitdev.hometrainer

import android.os.Bundle
import android.view.MenuItem
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import fr.fitdev.hometrainer.history.HistoryAdapter
import fr.fitdev.hometrainer.session.Session
import fr.fitdev.hometrainer.util.Constants
import fr.fitdev.hometrainer.util.Utils

class SessionHistoryActivity : AppCompatActivity() {

    lateinit var session: Session

    /**
     * Création de l'activité
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_session_history)

        // Récupération des informations
        session = intent.getParcelableExtra(Constants.EXTRA_SESSION)

        // Configuration de la toolbar
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        toolbar.title = "Historique ${session.name}"
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        // Configuration du l'adapteur et de son recyclerView
        val recyclerView = findViewById<RecyclerView>(R.id.history_recyclerview)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = HistoryAdapter(session.histories)

        // Affichage des informations
        findViewById<TextView>(R.id.histo_nb).text =
            "Séance réalisée ${session.histories.size} fois"

        findViewById<TextView>(R.id.histo_duree).text =
            "Durée totale: ${Utils.getDate(session.histories.stream().mapToLong { t -> t.end - t.start }.sum())}"
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == android.R.id.home) {
            this.onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}