package fr.fitdev.hometrainer

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import fr.fitdev.hometrainer.itemtouch.ItemTouchBuilder
import fr.fitdev.hometrainer.session.*
import fr.fitdev.hometrainer.popup.session.PopupCreateSession

/**
 * Activité principale
 */
class MainActivity : AppCompatActivity() {

    lateinit var sessions: ArrayList<Session>
    lateinit var sessionAdapter: SessionAdapter

    private lateinit var popupCreateSession: PopupCreateSession

    /**
     * Création de l'activité
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.toolbar))

        // Récupération de toutes les séances
        sessions = SessionManager.getInstance().sqlAdapter.findAll()

        // Configuration du l'adapteur et de son recyclerView
        sessionAdapter = SessionAdapter(sessions, SessionClickListener(this))
        val recyclerView = findViewById<RecyclerView>(R.id.session_recyclerview)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = sessionAdapter

        // Configuration de l'action delete pour les items du recyclerView
        ItemTouchBuilder(recyclerView).delete { viewHolder: RecyclerView.ViewHolder, _: Int ->
            val index: Int = viewHolder.adapterPosition
            val session = sessions[index]
            SessionManager.getInstance().sqlAdapter.delete(session)
            sessions.removeAt(index)
            sessionAdapter.notifyItemRemoved(index)
        }

        // Création popup de création d'une séance
        popupCreateSession = PopupCreateSession(this)
        findViewById<FloatingActionButton>(R.id.start_session).setOnClickListener { popupCreateSession.show() }
    }
}