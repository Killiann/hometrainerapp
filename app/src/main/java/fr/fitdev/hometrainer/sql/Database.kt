package fr.fitdev.hometrainer.sql

import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class Database(context: Context):
    SQLiteOpenHelper(context, "hometrainer.db", null, 1) {

    // https://www.sqlite.org/datatype3.html

    /**
     * Executé lors du 1er lancement de l'application
     */
    override fun onCreate(database: SQLiteDatabase?) {
        /*
        Création des tables
         */
        database?.execSQL("CREATE TABLE sessions(idSession INTEGER PRIMARY KEY AUTOINCREMENT, name VARCHAR(255), favorite TINYINT)")
        database?.execSQL("CREATE TABLE block(idBock INTEGER PRIMARY KEY AUTOINCREMENT, name VARCHAR(255), repetition INT)")
        database?.execSQL("CREATE TABLE exercises(idExercise INTEGER PRIMARY KEY AUTOINCREMENT, name VARCHAR(255), duration INT, power INT, frequency INT, repetition INT)")
        database?.execSQL("CREATE TABLE component(idComponent INTEGER PRIMARY KEY AUTOINCREMENT, indexComponent INT, idBock INT, idExercise INT, idSession INT, FOREIGN KEY(idBock) REFERENCES block(idBock) ON DELETE CASCADE, FOREIGN KEY(idExercise) REFERENCES exercises(idExercise) ON DELETE CASCADE, FOREIGN KEY(idSession) REFERENCES sessions(idSession) ON DELETE CASCADE)")
        database?.execSQL("CREATE TABLE histories(startHistory INT PRIMARY KEY, complete TINYINT, pauses INT, endHistory INT, idSession INT, FOREIGN KEY(idSession) REFERENCES sessions(idSession) ON DELETE CASCADE) WITHOUT ROWID")

        /*
        SEANCE 1
         */
        database?.execSQL("INSERT INTO sessions VALUES(1, 'Séance 1', 0)")
        database?.execSQL("INSERT INTO exercises VALUES(1, 'Echauffement', 15, 150, 0, 1)")
        database?.execSQL("INSERT INTO component VALUES(1, 0, NULL, 1, 1)")

        database?.execSQL("INSERT INTO block VALUES(1, 'Groupe 1', 3)")
        database?.execSQL("INSERT INTO component VALUES(2, 1, 1, NULL, 1)")
        database?.execSQL("INSERT INTO exercises VALUES(2, 'Effort modérée', 10, 290, 90, 1)")
        database?.execSQL("INSERT INTO exercises VALUES(3, 'Récupération', 3, 180, 0, 1)")
        database?.execSQL("INSERT INTO component VALUES(3, 0, 1, 2, 1)")
        database?.execSQL("INSERT INTO component VALUES(4, 1, 1, 3, 1)")

        database?.execSQL("INSERT INTO block VALUES(2, 'Groupe 2', 3)")
        database?.execSQL("INSERT INTO component VALUES(5, 2, 2, NULL, 1)")
        database?.execSQL("INSERT INTO exercises VALUES(4, 'Effort élévée', 10, 300, 110, 1)")
        database?.execSQL("INSERT INTO exercises VALUES(5, 'Récupération', 1, 150, 0, 1)")
        database?.execSQL("INSERT INTO component VALUES(6, 0, 2, 4, 1)")
        database?.execSQL("INSERT INTO component VALUES(7, 1, 2, 5, 1)")

        database?.execSQL("INSERT INTO exercises VALUES(6, 'Récupération', 10, 150, 0, 1)")
        database?.execSQL("INSERT INTO component VALUES(8, 3, NULL, 6, 1)")

        /*
         SEANCE 2
         */
        database?.execSQL("INSERT INTO sessions VALUES(2, 'Séance 2', 0)")
        database?.execSQL("INSERT INTO exercises VALUES(7, 'Echauffement', 15, 150, 0, 1)")
        database?.execSQL("INSERT INTO component VALUES(9, 0, NULL, 7, 2)")

        database?.execSQL("INSERT INTO block VALUES(3, 'Groupe 1', 3)")
        database?.execSQL("INSERT INTO component VALUES(10, 1, 3, NULL, 2)")
        database?.execSQL("INSERT INTO exercises VALUES(8, 'Effort modérée', 2, 350, 0, 1)")
        database?.execSQL("INSERT INTO exercises VALUES(9, 'Récupération', 5, 180, 0, 1)")
        database?.execSQL("INSERT INTO component VALUES(11, 0, 3, 8, 2)")
        database?.execSQL("INSERT INTO component VALUES(12, 1, 3, 9, 2)")

        database?.execSQL("INSERT INTO block VALUES(4, 'Groupe 2', 5)")
        database?.execSQL("INSERT INTO component VALUES(13, 2, 4, NULL, 2)")
        database?.execSQL("INSERT INTO exercises VALUES(10, 'Effort élévée', 1, 380, 0, 1)")
        database?.execSQL("INSERT INTO exercises VALUES(11, 'Récupération', 1, 150, 0, 1)")
        database?.execSQL("INSERT INTO component VALUES(14, 0, 4, 10, 2)")
        database?.execSQL("INSERT INTO component VALUES(15, 1, 4, 11, 2)")

        database?.execSQL("INSERT INTO exercises VALUES(12, 'Récupération', 10, 150, 0, 1)")
        database?.execSQL("INSERT INTO component VALUES(16, 3, NULL, 12, 2)")
    }

    override fun onUpgrade(p0: SQLiteDatabase?, p1: Int, p2: Int) {}

    /**
     * Retourne un string stocké dans [cursor] avec comme clé [key]
     */
    fun getString(cursor: Cursor, key: String): String = cursor.getString(cursor.getColumnIndex(key))

    /**
     * Retourne un int stocké dans [cursor] avec comme clé [key]
     */
    fun getInt(cursor: Cursor, key: String): Int = cursor.getInt(cursor.getColumnIndex(key))

    /**
     * Retourne un long stocké dans [cursor] avec comme clé [key]
     */
    fun getLong(cursor: Cursor, key: String): Long = cursor.getLong(cursor.getColumnIndex(key))
}