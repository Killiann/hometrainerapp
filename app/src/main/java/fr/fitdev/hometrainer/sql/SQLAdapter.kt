package fr.fitdev.hometrainer.sql

import java.lang.RuntimeException

/**
 * Classe anbstraite qui permet de mettre en place un adapteur SQL
 * Cette classe à un accès direct à la base de données bia [database]
 *
 * @throws RuntimeException si la base de données n'est pas valide
 */
abstract class SQLAdapter<T, K>(
    protected val database: Database = if (SQLManager.getInstance() == null) throw RuntimeException(
        "Eroor: SQLManager Initialization"
    ) else SQLManager.getInstance()!!.database
) {

    /**
     * Insérer [generic] dans la [database]
     */
    abstract fun insert(generic: T)

    /**
     * Mettre à jour [generic] dans la [database]
     */
    abstract fun update(generic: T)

    /**
     * Supprimer [generic] dans la [database]
     */
    abstract fun delete(generic: T)

    /**
     * Trouver [T] dans la [database] en foction de [generic]
     */
    abstract fun find(generic: K): T?

    /**
     * Retourne une liste de [T]
     */
    abstract fun findAll(): ArrayList<T>
}