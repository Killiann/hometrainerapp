package fr.fitdev.hometrainer.sql

import android.content.Context

class SQLManager private constructor(context: Context) {

    /**
     * Partie statique du sigleton [SQLManager]
     */
    companion object {

        private var instance: SQLManager? = null

        /**
         * Retourne l'instance de [SQLManager]
         */
        fun getInstance(context: Context): SQLManager {
            if (instance == null)
                instance = SQLManager(context)
            return instance!!
        }

        /**
         * Retourne l'instance de [SQLManager]
         */
        fun getInstance(): SQLManager? {
            return instance
        }
    }

    // Init Database
    val database: Database = Database(context)
}