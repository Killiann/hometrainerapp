package fr.fitdev.hometrainer

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import fr.fitdev.hometrainer.history.History
import fr.fitdev.hometrainer.util.Constants
import fr.fitdev.hometrainer.util.Utils
import java.text.SimpleDateFormat

class SessionEndActivity : AppCompatActivity() {

    lateinit var history: History

    /**
     * Création de l'activité
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_session_end)
        setSupportActionBar(findViewById(R.id.toolbar))

        // Récupération des informations
        history = intent.getParcelableExtra(Constants.EXTRA_HISTORY)

        // Affichage des informations
        findViewById<TextView>(R.id.session_end_pause).text =
            "Vous avez fait ${history.pauses} ${if (history.pauses > 1) "pauses" else "pause"}"

        findViewById<TextView>(R.id.session_end_date).text =
            "Commencé le ${SimpleDateFormat("dd/MM/yyyy").format(history.start)}"

        findViewById<TextView>(R.id.session_end_hour).text =
            "A ${SimpleDateFormat("HH:mm").format(history.start)}"

        findViewById<TextView>(R.id.session_end_duration).text =
            "Durée: ${Utils.getDate(history.end - history.start)}"

        // Quand l'utilisateur clique sur le bouton 'Terminer' on l'envoie sur [MainActivity]
        findViewById<Button>(R.id.button_stop).setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
        }
    }
}