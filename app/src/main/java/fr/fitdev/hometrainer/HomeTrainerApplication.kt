package fr.fitdev.hometrainer

import android.app.Application
import fr.fitdev.hometrainer.sql.SQLManager
import java.lang.Exception

/**
 * Première classe instancié de l'application
 */
class HomeTrainerApplication : Application() {

    /**
     * Création de l'application
     */
    override fun onCreate() {
        super.onCreate()

        try {
            // Init la base de données
            SQLManager.getInstance(this)
        } catch (e: Exception) {
            throw Exception("Error: SQLManager init / connection", e)
        }
    }
}