package fr.fitdev.hometrainer.session

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.view.marginStart
import androidx.recyclerview.widget.RecyclerView
import fr.fitdev.hometrainer.R
import fr.fitdev.hometrainer.itemtouch.delete.DeletableView
import fr.fitdev.hometrainer.util.Utils

/**
 * Adapteur qui permet de remplir les information des [sessions] dans les item_session pour l'affichage
 *
 * @property sessions Liste des séances
 * @property listener Canal d'écoute appelé quand l'utilisateur appuit sur une séance
 */
class SessionAdapter(
    private val sessions: List<Session>,
    private val listener: View.OnClickListener
) : RecyclerView.Adapter<SessionView>() {

    /**
     * Création de la vue en fonction du layout item_session
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SessionView {
        val viewItem = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_session, parent, false)
        return SessionView(viewItem)
    }

    /**
     * Méthode qui définit les informations des [sessions]
     */
    override fun onBindViewHolder(holder: SessionView, position: Int) {
        val session = sessions[position]

        holder.cardView.setOnClickListener(listener)
        holder.cardView.tag = position
        holder.name.text = session.name
        holder.time.text = Utils.getDateWithoutSeconds(session.getDurationInMillis())
        if (session.favorite) {
            holder.favorite.visibility = View.VISIBLE

            // Pas le plus propre, il faudrait faire un linear layout à la place d'un relative
            val layoutParams = holder.name.layoutParams as RelativeLayout.LayoutParams
            layoutParams.setMargins(90, 0, 0, 0)
            holder.name.layoutParams = layoutParams
        }
    }

    /**
     * Retourne le nombre de [sessions]
     */
    override fun getItemCount(): Int = sessions.size
}

/**
 * Classe qui stocke la liste des éléments dans le layout item_session
 */
class SessionView(view: View) : DeletableView(view) {
    val cardView: CardView = view.findViewById(R.id.card_view)
    val name: TextView = view.findViewById(R.id.session_name)
    val favorite: ImageView = view.findViewById(R.id.favorite_icon)
    val time: TextView = view.findViewById(R.id.time)
}