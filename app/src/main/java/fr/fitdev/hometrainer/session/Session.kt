package fr.fitdev.hometrainer.session

import android.os.Parcel
import android.os.Parcelable
import fr.fitdev.hometrainer.component.Component
import fr.fitdev.hometrainer.history.History
import fr.fitdev.hometrainer.util.Composer
import java.util.ArrayList

/**
 * @property id Id de la séance
 * @property name Nom de la séance
 * @property favorite si la séance est en favorie
 * @property components Liste des components
 * @property histories Liste des historiques
 */
data class Session(
    var id: Int,
    var name: String?,
    var favorite: Boolean,
    var components: ArrayList<Component> = ArrayList(),
    var histories: ArrayList<History> = ArrayList()
) : Composer<Component>, Parcelable {

    /**
     * Crée une séance en fonction d'un [parcel]
     */
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString(),
        parcel.readByte() != 0.toByte(),
        parcel.createTypedArrayList(Component)!!,
        parcel.createTypedArrayList(History)!!
    )

    /**
     * Sérialise une séance
     */
    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(name)
        parcel.writeInt(if (favorite) 1 else 0)
        parcel.writeTypedList(components)
        parcel.writeTypedList(histories)
    }

    override fun describeContents(): Int {
        return 0
    }

    /**
     * Ajoute un Component [generic] à la séance
     */
    override fun add(generic: Component) {
        components.add(generic)
    }

    /**
     * Retourne le nombre de components dans la séance
     */
    override fun size(): Int {
        return components.size
    }

    /**
     * Retourne l'id de la séance
     */
    override fun getSession(): Int {
        return id
    }

    fun getDurationInMillis(): Long {
        return components.stream().mapToLong { t -> t.getDurationInMillis() }.sum()
    }

    /**
     * Méthodes statiques qui font appel au constructeur de la classe [Session]
     */
    companion object CREATOR : Parcelable.Creator<Session> {
        override fun createFromParcel(parcel: Parcel): Session {
            return Session(parcel)
        }

        override fun newArray(size: Int): Array<Session?> {
            return arrayOfNulls(size)
        }
    }
}