package fr.fitdev.hometrainer.session

import android.content.ContentValues
import android.database.Cursor
import fr.fitdev.hometrainer.component.ComponentSQLAdapter
import fr.fitdev.hometrainer.component.ComponentManager
import fr.fitdev.hometrainer.history.HistoryManager
import fr.fitdev.hometrainer.history.HistorySQLAdapter
import fr.fitdev.hometrainer.sql.SQLAdapter

/**
 * Classe qui intègre toutes les actions d'une séance avec la base de données
 */
class SessionSQLAdapter: SQLAdapter<Session, String>() {

    private val componentSQLAdapter: ComponentSQLAdapter = ComponentManager.getInstance().componentSQLAdapter
    private val historyAdapter: HistorySQLAdapter = HistoryManager.getInstance().sqlAdapter

    /**
     * Insérer une séance [generic] en base de données
     */
    override fun insert(generic: Session) {
        val values = ContentValues()
        values.put("name", generic.name)
        values.put("favorite", 0)
        generic.id = database.writableDatabase.insert("sessions", null, values).toInt()
    }

    /**
     * Mettre à jour une séance [generic] en base de données
     */
    override fun update(generic: Session) {
        database.writableDatabase.execSQL("UPDATE sessions SET name = ?, favorite = ? WHERE idSession = ?",
            arrayOf(generic.name, generic.favorite, generic.id))
    }

    /**
     * Suppression d'une séance [generic]
     */
    override fun delete(generic: Session) {
        database.writableDatabase.execSQL("DELETE FROM sessions WHERE name = ?", arrayOf(generic.name))
    }

    /**
     * Retourne une séance trouvé en base de données grâce à son nom
     */
    override fun find(generic: String): Session? {
        database.readableDatabase.rawQuery("SELECT * FROM sessions WHERE name = ?", arrayOf(generic)).use { cursor ->
            if (cursor.moveToNext()) {
                return cursorToSession(cursor)
            }
        }
        return null
    }

    /**
     * Retoute la liste de toutes les séance trouvé en base de données
     */
    override fun findAll(): ArrayList<Session> {
        val sessions = ArrayList<Session>()

        database.readableDatabase.rawQuery("SELECT * FROM sessions ORDER BY favorite DESC", null).use { cursor ->
            while (cursor.moveToNext()) {
                sessions.add(cursorToSession(cursor))
            }
        }
        return sessions
    }

    /**
     * Retourne une session construite à partir du résultat d'une requête
     */
    private fun cursorToSession(cursor: Cursor): Session{
        val id = database.getInt(cursor, "idSession")
        return Session(id, database.getString(cursor, "name"),
            database.getInt(cursor, "favorite")==1, componentSQLAdapter.findAll(id), historyAdapter.findAll(id))
    }
}