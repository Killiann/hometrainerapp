package fr.fitdev.hometrainer.session

/**
 * Manager qui permet de gérer toutes les intéraction avec la classe [Session]
 *
 * @property sqlAdapter Adapteur SQL des séances
 */
class SessionManager private constructor(val sqlAdapter: SessionSQLAdapter = SessionSQLAdapter()) {

    /**
     * Partie statique du sigleton [SessionManager]
     */
    companion object {

        private var instance: SessionManager? = null

        /**
         * Retourne l'instance de [SessionManager]
         */
        fun getInstance(): SessionManager {
            if (instance == null)
                instance = SessionManager()
            return instance!!
        }
    }
}