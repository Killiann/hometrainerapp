package fr.fitdev.hometrainer.session

import android.content.Intent
import android.util.Log
import android.view.View
import fr.fitdev.hometrainer.util.Constants
import fr.fitdev.hometrainer.MainActivity
import fr.fitdev.hometrainer.SessionDetailActivity

/**
 * Canal d'écoute quand l'utilisateur clique sur un item [Session]
 *
 * @property activity Activité d'où vient le signal
 */
class SessionClickListener(private val activity: MainActivity) : View.OnClickListener {

    override fun onClick(view: View) {
        // Si la view est bien une séance
        if (view.tag != null) {
            val index = view.tag as Int

            // Création d'un Intent(Classe instancié pour créer une nouvelle activité) avec les informations de la séance
            val intent = Intent(activity, SessionDetailActivity::class.java)
            intent.putExtra(Constants.EXTRA_SESSION, activity.sessions[index])
            activity.startActivity(intent)
        }
    }
}