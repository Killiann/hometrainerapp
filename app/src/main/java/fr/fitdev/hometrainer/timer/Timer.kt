package fr.fitdev.hometrainer.timer

import android.os.CountDownTimer
import fr.fitdev.hometrainer.SessionProgressActivity

/**
 * Classe utilitaire pour gérer le timer d'un exercice
 *
 * @property timeInSeconds Durée du timer
 * @property activity Activité de la séance en cours
 */
class Timer(private val timeInSeconds: Long, private val activity: SessionProgressActivity) {

    var timerState = TimerState.STOP
    private var secondsRemaining = 0L

    private lateinit var timer: CountDownTimer

    /**
     * Lancer le timer
     */
    fun startTimer() {
        timerState = TimerState.PROGRESS
        timer = createCountDownTimer().start()
    }

    /**
     * Arrêter le timer et lancer le prochain exercice
     */
    fun finishTimer() {
        timerState = TimerState.STOP
        secondsRemaining = timeInSeconds

        activity.progressCountdown.progress = 0
        timer.cancel()

        updateCountDownUI()

        activity.timeBelow += timeInSeconds
        activity.startNewExercise()
    }

    /**
     * Mettre en pause le timer
     */
    fun pauseTimer(): Boolean {
        if (timerState == TimerState.PROGRESS) {
            timerState = TimerState.PAUSE
            timer.cancel()
            return true
        }

        return false
    }

    /**
     * Initialiser le timer
     */
    fun initTimer() {
        if (timerState == TimerState.STOP) {
            secondsRemaining = timeInSeconds
            activity.progressCountdown.max = timeInSeconds.toInt()
        }

        if (timerState == TimerState.PROGRESS) {
            timer.cancel()
            startTimer()
        }

        updateCountDownUI()
    }

    /**
     * Mettre à jour le temps restant de l'exercice ainsi que la progress bar
     */
    private fun updateCountDownUI() {
        activity.progressText.text = toString()
        activity.progressCountdown.progress = (timeInSeconds - secondsRemaining).toInt()

        activity.sessionText.text = toStringSessionTimer()
        activity.sessionCountdown.progress = (activity.timeBelow + (timeInSeconds - secondsRemaining)).toInt()
    }

    /**
     * Créer un [CountDownTimer] qui se répète toutes les 1 seconde
     */
    private fun createCountDownTimer(): CountDownTimer =
        object : CountDownTimer(secondsRemaining * 1000, 1000) {
            override fun onFinish() = finishTimer()

            override fun onTick(millisUntilFinished: Long) {
                secondsRemaining = millisUntilFinished / 1000
                updateCountDownUI()
            }
        }

    /**
     * Formatte le temps restant du timer
     */
    override fun toString(): String {
        val minutesUntilFinished = secondsRemaining / 60
        val secondsStr = (secondsRemaining - minutesUntilFinished * 60).toString()
        return "$minutesUntilFinished:${if (secondsStr.length == 2) secondsStr else "0$secondsStr"}"
    }

    fun toStringSessionTimer(): String {
        val secondsUntilFinished = activity.timeBelow + (timeInSeconds - secondsRemaining)
        val minutesUntilFinished = secondsUntilFinished / 60
        val secondsStr = (secondsUntilFinished - minutesUntilFinished * 60).toString()
        return "$minutesUntilFinished:${if (secondsStr.length == 2) secondsStr else "0$secondsStr"}"
    }
}