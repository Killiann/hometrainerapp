package fr.fitdev.hometrainer.timer

/**
 * Enumération de l'état du timer
 */
enum class TimerState {
    STOP, PAUSE, PROGRESS
}