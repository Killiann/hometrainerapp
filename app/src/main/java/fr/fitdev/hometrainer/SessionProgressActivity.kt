package fr.fitdev.hometrainer

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.view.View
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.cardview.widget.CardView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import fr.fitdev.hometrainer.component.block.Block
import fr.fitdev.hometrainer.component.exercise.Exercise
import fr.fitdev.hometrainer.history.History
import fr.fitdev.hometrainer.history.HistoryManager
import fr.fitdev.hometrainer.session.Session
import fr.fitdev.hometrainer.popup.timer.PopupPause
import fr.fitdev.hometrainer.timer.Timer
import fr.fitdev.hometrainer.util.Constants
import fr.fitdev.hometrainer.util.Utils
import me.zhanghai.android.materialprogressbar.MaterialProgressBar
import java.util.function.ToIntFunction
import kotlin.collections.ArrayList
import kotlin.concurrent.timer

class SessionProgressActivity : AppCompatActivity() {

    private lateinit var popupPause: PopupPause
    private lateinit var session: Session
    private lateinit var history: History
    lateinit var timer: Timer

    lateinit var progressCountdown: MaterialProgressBar
    lateinit var progressText: TextView
    lateinit var exerciseText: TextView
    lateinit var repetitionText: TextView
    lateinit var powerText: TextView
    lateinit var frequencyText: TextView

    private var exercises = ArrayList<Exercise>()
    private var exerciseIndex = 0
    private var repetition = 0

    private var lastSkip = 0L

    lateinit var sessionCountdown: MaterialProgressBar
    lateinit var sessionText: TextView
    var time: Long = 0
    var timeBelow: Long = 0L

    /**
     * Création de l'activité
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // On affiche le layout
        setContentView(R.layout.activity_session_progress)

        // Récupération des informations
        session = intent.getParcelableExtra(Constants.EXTRA_SESSION)
        history = History(session.id)

        // On configure la toolbar
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)

        popupPause = PopupPause(this)

        progressCountdown = findViewById(R.id.progress_countdown)
        sessionCountdown = findViewById(R.id.progress_countdown_session)
        sessionText = findViewById(R.id.session_time)
        progressText = findViewById(R.id.countdown)
        exerciseText = findViewById(R.id.actual_exercise)
        repetitionText = findViewById(R.id.actual_repetition)
        powerText = findViewById(R.id.actual_power)
        frequencyText = findViewById(R.id.actual_frequency)

        findViewById<FloatingActionButton>(R.id.progress_pause).setOnClickListener { showPause() }

        findViewById<FloatingActionButton>(R.id.progress_next).setOnClickListener {
            if(lastSkip < System.currentTimeMillis()) {
                lastSkip = System.currentTimeMillis() + 200L
                timer.finishTimer()
            }
        }

        //Génération de la liste des exercices
        session.components.forEach { component ->
            if (component is Block) {
                for (i in 1..component.repetition) {
                    exercises.addAll(component.exercises as ArrayList<out Exercise>) // A update si on veut que les groupes puissent contenir des groupes
                }
            } else if (component is Exercise) {
                exercises.add(component)
            }
        }

        time = exercises.stream().mapToLong { t -> t.duration * 60L * t.repetition }.sum()
        sessionCountdown.max = time.toInt()

        // Lancement d'un exercice
        startNewExercise(false)
        createCountDownTimer().start()
    }

    /**
     * Appelé quand l'utilisateur revient sur l'application
     */
    override fun onResume() {
        super.onResume()
        if(::timer.isInitialized) {
            timer.initTimer()
        }
    }

    /**
     * Appelé quand l'utilisateur quitte l'application sans la fermer
     */
    override fun onPause() {
        super.onPause()
        showPause()
    }

    /**
     * Appelé quand l'utilisateur quitte l'ativité en cours
     */
    override fun finish() {
        endHistory()
        super.finish()
    }

    /**
     * Lancer le prochain exercice
     */
    fun startNewExercise() {
        startNewExercise(true)
    }

    fun startNewExercise(time: Boolean) {
        if(exerciseIndex >= exercises.size) return

        val exercise = getNewExercise()
        updateNextExercise()

        // Si exercise == null c'est que la séance est terminé
        if (exercise == null) {
            // Sauvegarde historique
            history.complete = true
            endHistory()

            // Affichage activité de fin de séance
            val intent = Intent(this, SessionEndActivity::class.java)
            intent.putExtra(Constants.EXTRA_HISTORY, history)
            startActivity(intent)
            return
        }
        repetition++

        exerciseText.text = exercise.name
        powerText.text = "${exercise.power} W"
        frequencyText.text = "${if (exercise.frequency > 0) "${exercise.frequency} Tr/min" else ""}"
        updateRepetitionTextView(exercise)

        // Lancement nouveau timer
        if(time) {
            timer = Timer(exercise.duration * 60L, this)
            timer.initTimer()
            timer.startTimer()
        }

        // Modification couleur en fonction de l'exercice
        findViewById<CardView>(R.id.exercise_progress_cardview).setCardBackgroundColor(
            baseContext.getColor(
                Utils.getColorId(exercise.power)
            )
        )
    }

    /**
     * Retoure le prochain exercice ou null si il n'y en a pas
     */
    fun getNewExercise(): Exercise? {
        val exercise = exercises[exerciseIndex]

        if (repetition >= exercise.repetition) {
            exerciseIndex++
            repetition = 0

            return if (exercises.size == exerciseIndex) null else exercises[exerciseIndex]
        }

        return exercise
    }

    /**
     * Met à jour l'affichage du nombre de répétition dans la carview de l'activité
     */
    private fun updateRepetitionTextView(exercise: Exercise) {
        //val difference = exercise.repetition - repetition
        repetitionText.text = "$repetition/${exercise.repetition}"
        /*if (difference > 0) {
            repetitionText.visibility = View.VISIBLE
            repetitionText.text =
                "Encore $difference ${if (difference > 1) "répétitions" else "répétition"}"
        } else {
            repetitionText.visibility = View.INVISIBLE
        }*/
    }

    /**
     * Afficher une pause
     */
    private fun showPause() {
        popupPause.show()
        if (timer.pauseTimer()) history.pauses++
    }

    /**
     * Sauvegarder les données de cette séance
     */
    private fun endHistory() {
        popupPause.active = false
        history.end = System.currentTimeMillis()
        HistoryManager.getInstance().sqlAdapter.insert(history)
    }

    private fun updateNextExercise() {
        val nextIndex = exerciseIndex + 1
        if(nextIndex >= exercises.size) {
            findViewById<CardView>(R.id.exercise_next_cardview).visibility = View.GONE
            return
        }

        val exercise = exercises[nextIndex]

        findViewById<CardView>(R.id.exercise_next_cardview).setCardBackgroundColor(
            baseContext.getColor(
                Utils.getColorId(exercise.power)
            )
        )

        findViewById<TextView>(R.id.next_exercise).text = exercise.name
        findViewById<TextView>(R.id.next_power).text = "${exercise.power} W"
        findViewById<TextView>(R.id.next_frequency).text = "${if (exercise.frequency > 0) "${exercise.frequency} Tr/min" else ""}"
        findViewById<TextView>(R.id.next_repetition).text = "0/${exercise.repetition}"
    }

    private fun createCountDownTimer(): CountDownTimer =
        object : CountDownTimer( 6000, 1000) {
            override fun onFinish() {
                exerciseIndex = 0
                repetition = 0
                supportActionBar?.title = "Séance en cours"
                startNewExercise()
            }

            override fun onTick(millisUntilFinished: Long) {
                progressText.text = "${millisUntilFinished / 1000}"
            }
        }
}