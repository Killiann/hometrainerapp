package fr.fitdev.hometrainer.component

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TableLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import fr.fitdev.hometrainer.HomeTrainerApplication
import fr.fitdev.hometrainer.R
import fr.fitdev.hometrainer.component.block.Block
import fr.fitdev.hometrainer.component.block.BlockClickListener
import fr.fitdev.hometrainer.component.exercise.Exercise
import fr.fitdev.hometrainer.component.exercise.ExerciseClickListener
import fr.fitdev.hometrainer.itemtouch.delete.DeletableView
import fr.fitdev.hometrainer.util.Utils

/**
 * Adapteur qui permet de remplir les information des [components] dans les item_exercise pour l'affichage
 *
 * @property components Liste des components (Block ou Exercise)
 * @property exerciseClickListener Canal d'écoute pour les exercices
 * @property blockClickListener Canal d'écoute pour les blocs
 */
class ComponentAdapter(
    private val components: List<Component>,
    private val exerciseClickListener: ExerciseClickListener<*, *>,
    private val blockClickListener: BlockClickListener<*, *>
) : RecyclerView.Adapter<ComponentView>() {

    /**
     * Création de la vue en fonction du layout item_exercise
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ComponentView {
        val viewItem = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_exercise, parent, false)
        return ComponentView(viewItem)
    }

    /**
     * Méthode qui définit les informations des [components] (exercice ou bloc)
     */
    override fun onBindViewHolder(holder: ComponentView, position: Int) {
        val component = components[position]

        holder.cardView.tag = position
        holder.name.text = component.name

        // On affiche la TextView de la répétition uniquement si elle est supérieur à 1
        if (component.repetition > 1) {
            holder.repetiton.visibility = View.VISIBLE
            holder.repetiton.text = "${component.repetition}x"
        } else {
            holder.repetiton.visibility = View.INVISIBLE
        }

        if (component is Exercise) {
            holder.cardView.setOnClickListener(exerciseClickListener)
            holder.minutes.text = "${component.duration} min"
            holder.power.text = "${component.power} W"
            holder.frequency.text = "${component.frequency} Tr/min"
            holder.table.visibility = View.VISIBLE
            holder.color.setBackgroundColor(holder.color.context.getColor(Utils.getColorId(component.power)))
        } else if (component is Block){
            holder.cardView.setOnClickListener(blockClickListener)
            holder.table.visibility = View.GONE

            // Pas le plus propre -> créer une class Comparator
            holder.color.setBackgroundColor(holder.color.context.getColor(Utils.getColorId(component.exercises.stream()
                .sorted { component1, component2 -> (component2 as Exercise).power-(component1 as Exercise).power }
                .mapToInt {component1 -> (component1 as Exercise).power}.findFirst().orElse(0))))
        }
    }

    /**
     * Retourne le nombre de [components]
     */
    override fun getItemCount(): Int = components.size
}

/**
 * Classe qui stocke la liste des éléments dans le layout item_exercise
 */
class ComponentView(view: View) : DeletableView(view) {
    val cardView: CardView = view.findViewById(R.id.exercise_card_view)
    val name: TextView = view.findViewById(R.id.exercise_name)
    val minutes: TextView = view.findViewById(R.id.exercise_minutes)
    val power: TextView = view.findViewById(R.id.exercise_power)
    val frequency: TextView = view.findViewById(R.id.exercise_frequency)
    val repetiton: TextView = view.findViewById(R.id.exercise_repetiton)
    val table: TableLayout = view.findViewById(R.id.table)
    val color: TextView = view.findViewById(R.id.color)
}