package fr.fitdev.hometrainer.component.block

import android.content.ContentValues
import android.util.Log
import fr.fitdev.hometrainer.sql.SQLAdapter
import java.lang.UnsupportedOperationException

/**
 * Classe qui intègre toutes les actions d'un bloc avec la base de données
 */
class BlockSQLAdapter: SQLAdapter<Block, Int>() {

    /**
     * Insérer un bloc [generic] en base de données
     */
    override fun insert(generic: Block) {
        // Insertion dans la table block
        var values = ContentValues()
        values.put("name", generic.name)
        values.put("repetition", generic.repetition)
        generic.id = database.writableDatabase.insert("block", null, values).toInt()

        // Insertion dans la table component pour faire la liaison avec la séance
        values = ContentValues()
        values.put("indexComponent", generic.index)
        values.put("idBock", generic.id)
        values.put("idSession", generic.sessionId)
        database.writableDatabase.insert("component", null, values)
    }

    /**
     * Mettre à jour le bloc [generic] en base de données
     */
    override fun update(generic: Block) {
        // Mise à jour des informations du bloc dans la table block
        database.writableDatabase.execSQL("UPDATE block SET name = ?, repetition = ? WHERE idBock = ?",
            arrayOf(generic.name, generic.repetition, generic.id))

        // Mise à jour des informations du bloc dans la table component
        database.writableDatabase.execSQL("UPDATE component SET indexComponent = ? WHERE idBock = ?",
            arrayOf(generic.index, generic.id))
    }

    /**
     * Suppression du bloc [generic]
     */
    override fun delete(generic: Block) {
        // Suppression du bloc dans la table block
        database.writableDatabase.execSQL("DELETE FROM block WHERE idBock = ?", arrayOf(generic.id))

        // Suppression du bloc dans la table component
        // Normalement avec le 'ON DELETE CASCADE' cette ligne ne sert à rien mais par sécurité on l'a met
        database.writableDatabase.execSQL("DELETE FROM component WHERE idBock = ?", arrayOf(generic.id))
    }

    /**
     * Retourne un bloc trouvé en base de données grâce à son id [generic]
     */
    override fun find(generic: Int): Block? {
        database.readableDatabase.rawQuery("SELECT * FROM block WHERE idBock = ?", arrayOf(generic.toString())).use { cursor ->
            // Si la requête retourne une ligne
            if (cursor.moveToNext()) {

                // Construction du bloc
                return Block(generic, -1,
                    database.getString(cursor, "name"),
                    database.getInt(cursor, "repetition"), -1)
            }
        }
        return null
    }

    /**
     * Cette fonction n'a pas été implémenté, elle n'est donc pas supporté
     */
    @Throws(UnsupportedOperationException::class)
    override fun findAll(): ArrayList<Block> {
        throw UnsupportedOperationException("Not supported")
    }
}