package fr.fitdev.hometrainer.component

import fr.fitdev.hometrainer.component.block.Block
import fr.fitdev.hometrainer.component.block.BlockSQLAdapter
import fr.fitdev.hometrainer.component.exercise.Exercise
import fr.fitdev.hometrainer.component.exercise.ExerciseSQLAdapter
import fr.fitdev.hometrainer.sql.SQLAdapter
import java.lang.UnsupportedOperationException

/**
 * Classe qui rassemble les actions d'un [Component] avec la base de données
 *
 * @property exerciseSQLAdapter Adapteur SQL des exercices
 * @property blockSQLAdapter Adapteur SQL des blocs
 */
class ComponentSQLAdapter(
    private val exerciseSQLAdapter: ExerciseSQLAdapter,
    private val blockSQLAdapter: BlockSQLAdapter
) : SQLAdapter<Component, Any>() {

    /**
     * Insérer un component [generic] en base de données
     */
    override fun insert(generic: Component) {
        if (generic is Block) {
            blockSQLAdapter.delete(generic)
        } else if(generic is Exercise) {
            exerciseSQLAdapter.delete(generic)
        }
    }

    /**
     * Mettre à jour un component [generic] en base de données
     */
    override fun update(generic: Component) {
        if (generic is Block) {
            blockSQLAdapter.update(generic)
        } else if(generic is Exercise) {
            exerciseSQLAdapter.update(generic)
        }
    }
    /**
     * Suppression d'un component [generic]
     */
    override fun delete(generic: Component) {
        if (generic is Block) {
            blockSQLAdapter.delete(generic)
        } else if(generic is Exercise) {
            exerciseSQLAdapter.delete(generic)
        }
    }

    /**
     * Retourne la liste des components d'une séance
     * Ces components sont automatiquement construit en fonction de leur type
     *
     * Si la colonne 'idBock' de la table component n'est pas null c'est que cette ligne représente
     * soit un bloc soit un exercice associé à un bloc
     */
    fun findAll(id: Int): ArrayList<Component> {
        val exercises = ArrayList<Component>()

        database.readableDatabase.rawQuery("SELECT * FROM component WHERE (idBock IS NULL OR idExercise IS NULL) AND idSession = ? ORDER BY indexComponent", arrayOf(id.toString())).use { cursor ->
            // Tant qu'il y a des lignes dans la requête
            while (cursor.moveToNext()) {
                val idBlock = database.getInt(cursor, "idBock")

                var component: Component
                if(idBlock > 0) {
                    component = blockSQLAdapter.find(idBlock)!!

                    // Récupère tous les exercices du block d'idBlock
                    (component as Block).exercises = exerciseSQLAdapter.findAll(component.id)
                } else{
                    component = exerciseSQLAdapter.find(database.getInt(cursor, "idExercise"))!!
                }

                component.index = database.getInt(cursor, "indexComponent")
                component.sessionId = database.getInt(cursor, "idSession")
                exercises.add(component)
            }
        }

        return exercises
    }

    /**
     * Cette fonction n'a pas été implémenté, elle n'est donc pas supporté
     */
    @Throws(UnsupportedOperationException::class)
    override fun find(generic: Any): Exercise? {
        throw UnsupportedOperationException("Not supported")
    }

    /**
     * Cette fonction n'a pas été implémenté, elle n'est donc pas supporté
     */
    @Throws(UnsupportedOperationException::class)
    override fun findAll(): ArrayList<Component> {
        throw UnsupportedOperationException("Not supported")
    }
}