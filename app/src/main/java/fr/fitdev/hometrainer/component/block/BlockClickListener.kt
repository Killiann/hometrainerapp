package fr.fitdev.hometrainer.component.block

import android.content.Intent
import android.view.View
import fr.fitdev.hometrainer.DetailActivity
import fr.fitdev.hometrainer.BlockDetailActivity
import fr.fitdev.hometrainer.component.Component
import fr.fitdev.hometrainer.util.Composer
import fr.fitdev.hometrainer.util.Constants

/**
 * Canal d'écoute quand l'utilisateur clique sur un item [Block]
 *
 * @param K composeur qui stocke le [T]
 * @param T component stocké dans [K]
 * @property activity Activité d'où vient le signal
 */
class BlockClickListener<K : Composer<T>, T : Component>(private val activity: DetailActivity<K, T>) :
    View.OnClickListener {

    /**
     * Ouvre l'activité détail d'un bloc si la [view] est valide
     */
    override fun onClick(view: View) {
        // Si la view est bien un bloc
        if (view.tag != null) {
            val index = view.tag as Int

            // Création d'un Intent(Classe instancié pour créer une nouvelle activité) avec les informations du bloc
            val intent = Intent(activity, BlockDetailActivity::class.java)
            intent.putExtra(Constants.EXTRA_BLOCK, activity.components[index] as Block)
            intent.putExtra(Constants.EXTRA_BLOCK_INDEX, index)
            activity.startActivityForResult(intent, 1)
        }
    }
}