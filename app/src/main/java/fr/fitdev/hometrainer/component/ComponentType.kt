package fr.fitdev.hometrainer.component

/**
 * Enumération des différents types d'un component
 */
enum class ComponentType {
    EXERCISE, BLOCK
}