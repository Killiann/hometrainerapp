package fr.fitdev.hometrainer.component

import android.os.Parcel
import android.os.Parcelable
import fr.fitdev.hometrainer.component.block.Block
import fr.fitdev.hometrainer.component.exercise.Exercise

/**
 * Classe abstraite mère de [Exercise], [Block]
 *
 * @property id Id du component
 * @property index Position du component
 * @property name Nom du component
 * @property repetition Nombre de répétition du bloc
 * @property sessionId Id de la sessions associé au component
 */
abstract class Component(
    var id: Int,
    var index: Int,
    var name: String?,
    var repetition: Int,
    var sessionId: Int
) : Parcelable {


    abstract fun getDurationInMillis(): Long

    /**
     * Méthodes statiques qui permettent de désérialiser des [Parcel] originaire d'un [Component]
     */
    companion object CREATOR : Parcelable.Creator<Component> {
        override fun createFromParcel(parcel: Parcel): Component {
            return if (parcel.readString().equals(ComponentType.EXERCISE.name)) Exercise(parcel) else Block(parcel)
        }

        override fun newArray(size: Int): Array<Component?> {
            return arrayOfNulls(size)
        }
    }
}