package fr.fitdev.hometrainer.component.exercise

import android.content.ContentValues
import fr.fitdev.hometrainer.component.Component
import fr.fitdev.hometrainer.sql.SQLAdapter
import java.lang.UnsupportedOperationException

/**
 * Classe qui intègre toutes les actions d'un exercice avec la base de données
 */
class ExerciseSQLAdapter: SQLAdapter<Exercise, Int>() {

    /**
     * Insérer un exercice [generic] en base de données
     */
    override fun insert(generic: Exercise) {
        // Insertion dans la table exercises
        var values = ContentValues()
        values.put("name", generic.name)
        values.put("duration", generic.duration)
        values.put("power", generic.power)
        values.put("frequency", generic.frequency)
        values.put("repetition", generic.repetition)
        generic.id = database.writableDatabase.insert("exercises", null, values).toInt()

        // Insertion dans la table component pour faire la liaison avec la séance
        values = ContentValues()
        values.put("indexComponent", generic.index)
        values.put("idExercise", generic.id)
        values.put("idSession", generic.sessionId)
        values.put("idBock", if (generic.blockId < 1) null else generic.blockId)
        database.writableDatabase.insert("component", null, values)
    }

    /**
     * Mettre à jour de l'exercice [generic] en base de données
     */
    override fun update(generic: Exercise) {
        // Mise à jour des informations de l'exercice dans la table exercises
        database.writableDatabase.execSQL("UPDATE exercises SET name = ?, duration = ?, power = ?, frequency = ?, repetition = ? WHERE idExercise = ?",
            arrayOf(generic.name, generic.duration, generic.power, generic.frequency, generic.repetition, generic.id))

        // Mise à jour des informations de l'exercice dans la table component
        database.writableDatabase.execSQL("UPDATE component SET indexComponent = ? WHERE idExercise = ?",
            arrayOf(generic.index, generic.id))
    }

    /**
     * Suppression de l'exercice [generic]
     */
    override fun delete(generic: Exercise) {
        // Suppression de l'exercice dans la table exercises
        database.writableDatabase.execSQL("DELETE FROM exercises WHERE idExercise = ?", arrayOf(generic.id))

        // Suppression de l'exercice dans la table component
        // Normalement avec le 'ON DELETE CASCADE' cette ligne ne sert à rien mais par sécurité on l'a met
        database.writableDatabase.execSQL("DELETE FROM component WHERE idExercise = ?", arrayOf(generic.id))
    }

    /**
     * Retourne un exercice trouvé en base de données grâce à son id [generic]
     */
    override fun find(generic: Int): Exercise? {
        database.readableDatabase.rawQuery("SELECT * FROM exercises WHERE idExercise = ?", arrayOf(generic.toString())).use { cursor ->
            // Si la requête retourne une ligne
            if (cursor.moveToNext()) {

                // Construction de l'exercice
                return Exercise(generic, -1,
                    database.getString(cursor, "name"),
                    database.getInt(cursor, "duration"),
                    database.getInt(cursor, "power"),
                    database.getInt(cursor, "frequency"),
                    database.getInt(cursor, "repetition"), -1, -1)
            }
        }
        return null
    }

    /**
     * Retourne la liste des exercices d'un bloc d'[id]
     */
    fun findAll(id: Int): ArrayList<Component> {
        val exercises = ArrayList<Component>()

        database.readableDatabase.rawQuery("SELECT e.*, c.indexComponent, c.idSession, c.idBock FROM exercises e, component c WHERE e.idExercise = c.idExercise AND c.idBock = ? ORDER BY c.indexComponent", arrayOf(id.toString())).use { cursor ->
            // Tant qu'il y a des lignes dans la requête
            while (cursor.moveToNext()) {

                // Construction de l'exercice
                val exercise = Exercise(database.getInt(cursor, "idExercise"),
                    database.getInt(cursor, "indexComponent"),
                    database.getString(cursor, "name"),
                    database.getInt(cursor, "duration"),
                    database.getInt(cursor, "power"),
                    database.getInt(cursor, "frequency"),
                    database.getInt(cursor, "repetition"),
                    database.getInt(cursor, "idBock"),
                    database.getInt(cursor, "idSession"))

                // Ajout de l'exercice dans la liste
                exercises.add(exercise)
            }
        }

        return exercises
    }

    /**
     * Cette fonction n'a pas été implémenté, elle n'est donc pas supporté
     */
    @Throws(UnsupportedOperationException::class)
    override fun findAll(): ArrayList<Exercise> {
        throw UnsupportedOperationException("Not supported")
    }
}