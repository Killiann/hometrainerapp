package fr.fitdev.hometrainer.component.exercise

import android.view.View
import fr.fitdev.hometrainer.DetailActivity
import fr.fitdev.hometrainer.component.Component
import fr.fitdev.hometrainer.component.block.Block
import fr.fitdev.hometrainer.popup.exercise.PopupUpdateExercise
import fr.fitdev.hometrainer.util.Composer

/**
 * Canal d'écoute quand l'utilisateur clique sur un item [Exercise]
 *
 * @param K composeur qui stocke le [T]
 * @param T component stocké dans [K]
 * @property activity Activité d'où vient le signal
 */
class ExerciseClickListener<K : Composer<T>, T : Component>(private val activity: DetailActivity<K, T>) :
    View.OnClickListener {

    var popupUpdate = PopupUpdateExercise(activity)

    override fun onClick(view: View) {
        // Si la view est bien un exercice
        if (view.tag != null) {
            val index = view.tag as Int

            // Affiche le popup pour mettre à jour l'exercice
            popupUpdate.show(activity.components[index], index)
        }
    }
}