package fr.fitdev.hometrainer.component.exercise

import android.os.Parcel
import android.os.Parcelable
import fr.fitdev.hometrainer.component.Component
import fr.fitdev.hometrainer.component.ComponentType

/**
 * Un exercice d'une Session ou d'un Block
 *
 * @property id Id de l'exercice
 * @property index Position de l'exercice
 * @property name Nom de l'exercice
 * @property duration Durée de l'exercice en secondes
 * @property power Puissance de l'exercice en W
 * @property frequency Vitesse de l'exercice en Tr/min
 * @property repetition Nombre de répétition de l'exercice
 * @property blockId Id du block auquel il est affecté (-1 par défaut)
 * @property sessionId Id de la session associé à l'exercice
 */
class Exercise(
    id: Int,
    index: Int,
    name: String?,
    var duration: Int,
    var power: Int,
    var frequency: Int,
    repetition: Int,
    var blockId: Int,
    sessionId: Int
) : Component(id, index, name, repetition, sessionId) {

    /**
     * Crée un exercice en fonction d'un [parcel]
     */
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readInt(),
        parcel.readString(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt()
    )

    /**
     * Sérialise un exercice
     */
    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(ComponentType.EXERCISE.name)
        parcel.writeInt(id)
        parcel.writeInt(index)
        parcel.writeString(name)
        parcel.writeInt(duration)
        parcel.writeInt(power)
        parcel.writeInt(frequency)
        parcel.writeInt(repetition)
        parcel.writeInt(blockId)
        parcel.writeInt(sessionId)
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun getDurationInMillis(): Long {
        return duration * 60000L
    }

    /**
     * Méthodes statiques qui font appel à la gestion des objets sérialisés dans Component
     */
    companion object CREATOR : Parcelable.Creator<Exercise> {
        override fun createFromParcel(parcel: Parcel): Exercise {
            return Component.createFromParcel(parcel) as Exercise
        }

        override fun newArray(size: Int): Array<Exercise?> {
            return arrayOfNulls(size)
        }
    }

    /**
     * Retourne l'affichage de l'exercice dans l'IHM
     */
    override fun toString(): String =
        "$name, $power W${if (frequency > 0) ", $frequency Tr/min" else ""}"
}