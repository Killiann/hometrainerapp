package fr.fitdev.hometrainer.component

import fr.fitdev.hometrainer.component.block.BlockSQLAdapter
import fr.fitdev.hometrainer.component.exercise.ExerciseSQLAdapter

/**
 * Manager qui permet de gérer toutes les intéractions avec la base de données des components
 *
 * @property exerciseSQLAdapter Adapteur SQL des exercices
 * @property blockSQLAdapter Adapteur SQL des blocs
 * @property componentSQLAdapter Adapteur SQL qui permet la liaison entre [ExerciseSQLAdapter] et [BlockSQLAdapter]
 */
class ComponentManager private constructor(
    val exerciseSQLAdapter: ExerciseSQLAdapter = ExerciseSQLAdapter(),
    val blockSQLAdapter: BlockSQLAdapter = BlockSQLAdapter(),
    val componentSQLAdapter: ComponentSQLAdapter = ComponentSQLAdapter(exerciseSQLAdapter, blockSQLAdapter)
) {

    /**
     * Partie statique du sigleton [ComponentManager]
     */
    companion object {

        private var instance: ComponentManager? = null

        /**
         * Retourne l'instance de [ComponentManager]
         */
        fun getInstance(): ComponentManager {
            if (instance == null)
                instance = ComponentManager()
            return instance!!
        }
    }
}