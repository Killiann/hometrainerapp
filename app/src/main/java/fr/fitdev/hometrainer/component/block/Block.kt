package fr.fitdev.hometrainer.component.block

import android.os.Parcel
import android.os.Parcelable
import fr.fitdev.hometrainer.component.Component
import fr.fitdev.hometrainer.component.ComponentType
import fr.fitdev.hometrainer.component.exercise.Exercise
import fr.fitdev.hometrainer.util.Composer
import java.util.ArrayList

/**
 * Un groupe/bloc d'exercices [Exercise]
 *
 * @property id Id du bloc
 * @property index Position du bloc
 * @property name Nom du bloc
 * @property repetition Nombre de répétition du bloc
 * @property sessionId Id de la session associé au bloc
 * @property exercises Liste des exercices
 */
class Block(
    id: Int,
    index: Int,
    name: String?,
    repetition: Int,
    sessionId: Int,
    var exercises: ArrayList<Component> = ArrayList(),
) : Composer<Component>, Component(id, index, name, repetition, sessionId) {

    /**
     * Crée un bloc en fonction d'un [parcel]
     */
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readInt(),
        parcel.readString(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.createTypedArrayList(Component)!!
    )

    /**
     * Sérialise un bloc
     */
    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(ComponentType.BLOCK.name)
        parcel.writeInt(id)
        parcel.writeInt(index)
        parcel.writeString(name)
        parcel.writeInt(repetition)
        parcel.writeInt(sessionId)
        parcel.writeTypedList(exercises)
    }

    override fun describeContents(): Int {
        return 0
    }

    /**
     * Ajoute un exercice [generic] au bloc
     */
    override fun add(generic: Component) {
        exercises.add(generic)
    }

    /**
     * Retourne le nombre d'exercices dans le bloc
     */
    override fun size(): Int {
        return exercises.size
    }

    /**
     * Retourne la session associé au bloc
     */
    override fun getSession(): Int {
        return sessionId
    }


    override fun getDurationInMillis(): Long {
        return exercises.stream().mapToLong { t -> t.getDurationInMillis() }.sum()
    }

    /**
     * Méthodes statiques qui font appel à la gestion des objets sérialisés dans Component
     */
    companion object CREATOR : Parcelable.Creator<Block> {
        override fun createFromParcel(parcel: Parcel): Block {
            return Component.createFromParcel(parcel) as Block
        }

        override fun newArray(size: Int): Array<Block?> {
            return arrayOfNulls(size)
        }
    }
}